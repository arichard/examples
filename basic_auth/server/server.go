package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

type service struct {
	username string
	password string
}

func main() {
	logger := log.Default()
	srvr, err := newServer()
	if err != nil {
		logger.Fatal(err)
	}
	if err = srvr.ListenAndServe(); err != nil {
		logger.Fatal(err)
	}
	os.Exit(0)
}

func newServer() (*http.Server, error) {
	svc := service{
		username: "uname",
	}

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Password: ")
	var err error
	if svc.password, err = reader.ReadString('\n'); err != nil {
		return nil, err
	}
	svc.password = strings.TrimSpace(svc.password)

	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/public", publicHandler)
	serveMux.HandleFunc("/protected", svc.protectedHandler)
	return &http.Server{
		Addr:              ":8080",
		Handler:           serveMux,
		ReadHeaderTimeout: time.Second,
	}, nil
}

func publicHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "Hello world")
}

func (s *service) protectedHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	username, password, ok := r.BasicAuth()
	if !ok || username != s.username || password != s.password {
		w.Header().Set("WWW-Authenticate", `Basic realm="examples basic_auth", charset="UTF-8"`)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "Something secret")
}
