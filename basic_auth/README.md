## Basic Auth

*WARNING* HTTPS must be used to be secure, but for simplicity this example omits it

### References

* [Wikipedia](https://en.wikipedia.org/wiki/Basic_access_authentication)
* [RFC 7617](https://www.rfc-editor.org/rfc/rfc7617.html)

### Running

Start the server

```shell
$ go run ./server
...
```

In another shell, run the client

```shell
$ go run ./client
...
```
