package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

func main() {
	ctx := context.Background()
	if err := client(ctx); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	os.Exit(0)
}

func client(ctx context.Context) error {
	httpClient := http.Client{
		Timeout: time.Second,
	}
	const serverURL = "http://localhost:8080"

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, serverURL+"/public", http.NoBody)
	if err != nil {
		return err
	}
	if err = doAndPrintResp(&httpClient, req); err != nil {
		return err
	}

	if req, err = http.NewRequestWithContext(ctx, http.MethodGet, serverURL+"/protected", http.NoBody); err != nil {
		return err
	}
	if err = doAndPrintResp(&httpClient, req); err != nil {
		return err
	}

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Password: ")
	var password string
	if password, err = reader.ReadString('\n'); err != nil {
		return err
	}
	password = strings.TrimSpace(password)

	if req, err = http.NewRequestWithContext(ctx, http.MethodGet, serverURL+"/protected", http.NoBody); err != nil {
		return err
	}
	req.SetBasicAuth("uname", password)
	if err = doAndPrintResp(&httpClient, req); err != nil {
		return err
	}

	if req, err = http.NewRequestWithContext(ctx, http.MethodGet, serverURL+"/protected", http.NoBody); err != nil {
		return err
	}
	req.SetBasicAuth("uname~", password)
	if err = doAndPrintResp(&httpClient, req); err != nil {
		return err
	}

	if req, err = http.NewRequestWithContext(ctx, http.MethodGet, serverURL+"/protected", http.NoBody); err != nil {
		return err
	}
	req.SetBasicAuth("uname", password+"~")
	return doAndPrintResp(&httpClient, req)
}

func doAndPrintResp(httpClient *http.Client, req *http.Request) error {
	username, password, ok := req.BasicAuth()
	fmt.Println(req.Method, req.URL.Path, username, password, ok)
	resp, err := httpClient.Do(req)
	if err != nil {
		return err
	}
	fmt.Println(resp.StatusCode)
	for k, v := range resp.Header {
		fmt.Println(k, v)
	}
	if _, err = io.Copy(os.Stdout, resp.Body); err != nil {
		return err
	}
	if err = resp.Body.Close(); err != nil {
		return err
	}
	fmt.Println()
	return nil
}
