#!/bin/sh
set -e

GOLANGCI_LINT_VERSION='1.49.0'

# Pull in some go vars used below
eval "$(go env | grep -E '(CGO_ENABLED|GOHOSTARCH|GOHOSTOS)')"

info() {
    printf '\033[0;32m%s\033[0m\n' "$*"
}

error() {
    printf '\033[0;31m%s\033[0m\n' "$*"
}

cd -- "$(dirname "$(realpath "$0")")"

# Install dependencies
if [ ! -x bin/goimports ]; then
    info 'Installing goimports'
    CGO_ENABLED="1" GOARCH="$GOHOSTARCH" GOBIN="$PWD/bin" GOOS="$GOHOSTOS" go install golang.org/x/tools/cmd/goimports@latest
fi
if [ ! -x bin/golangci-lint ]; then
    info 'Installing golangci-lint'
    curl -sSL "https://github.com/golangci/golangci-lint/releases/download/v$GOLANGCI_LINT_VERSION/golangci-lint-$GOLANGCI_LINT_VERSION-$GOHOSTOS-$GOHOSTARCH.tar.gz" | tar -C bin -xzf - --strip-components=1 "golangci-lint-$GOLANGCI_LINT_VERSION-$GOHOSTOS-$GOHOSTARCH/golangci-lint"
fi

# go get -u -t -d ./...

info 'Mod tidy...'
go mod tidy

info 'Formatting go files...'
find ./* -type f -name '*.go' -print0 | xargs -0 bin/goimports -w

GO_BUILD_FLAGS="-v"
if ! command -v git > /dev/null 2>&1; then
    error 'Disabling build vcs'
    GO_BUILD_FLAGS="$GO_BUILD_FLAGS -buildvcs=false"
fi
info 'Compiling...'
go build $GO_BUILD_FLAGS ./...

GO_TEST_FLAGS=""
# -race requires CGO
if [ "$CGO_ENABLED" = '1' ]; then
    GO_TEST_FLAGS="$GO_TEST_FLAGS -race"
else
    error 'Disabling -race'
fi
info 'Testing...'
go test $GO_TEST_FLAGS ./...

info 'Linting...'
# use 1.17 until https://github.com/golangci/golangci-lint/issues/2649 is resolved
bin/golangci-lint run --go=1.17

echo OK
