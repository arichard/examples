#!/bin/sh
set -e

gen_cert() {
    openssl req -newkey ec \
        -pkeyopt ec_paramgen_curve:prime256v1 \
        -new -nodes -x509 \
        -days 90 \
        -out "$1$2cert.pem" \
        -keyout "$1$2key.pem" \
        -subj "/O=server $1 client $2/CN=$2.$1.localhost" \
        -addext "subjectAltName = DNS:$2.$1.localhost" \

}

rm -f *.cert
gen_cert s1 c1
gen_cert s2 c1
gen_cert s1 c2
# c2 is not allowed to talk to s2
