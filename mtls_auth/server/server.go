package main

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var errNoCertsFound = errors.New("no certs found")

func main() {
	logger := log.Default()
	srvr, err := newServer(logger, os.Args)
	if err != nil {
		logger.Fatal(err)
	}
	if err = srvr.ListenAndServeTLS("", ""); err != nil {
		logger.Fatal(err)
	}
	os.Exit(0)
}

func newServer(logger *log.Logger, args []string) (*http.Server, error) {
	cmdLine := flag.NewFlagSet(args[0], flag.ExitOnError)
	var serverName string
	cmdLine.StringVar(&serverName, "s", "s1", "server name")
	err := cmdLine.Parse(args[1:])
	if err != nil {
		return nil, err
	}
	if extraArgs := cmdLine.Args(); len(extraArgs) > 0 {
		fmt.Fprintf(cmdLine.Output(), "unexpected additional args: %v\n", extraArgs)
		cmdLine.Usage()
		os.Exit(2) //nolint:gomnd,revive // Not ideal, but OK for now
	}

	const certSuffix = "cert.pem"
	var names []string
	if names, err = filepath.Glob(serverName + "*" + certSuffix); err != nil {
		return nil, err
	}
	if len(names) == 0 {
		return nil, errNoCertsFound
	}
	logger.Printf("reading certs %v", names)
	caCertPool := x509.NewCertPool()
	certs := make([]tls.Certificate, len(names))
	for i, name := range names {
		var caCert []byte
		if caCert, err = os.ReadFile(name); err != nil {
			return nil, err
		}
		caCertPool.AppendCertsFromPEM(caCert)
		var keyCert []byte
		if keyCert, err = os.ReadFile(strings.TrimSuffix(name, certSuffix) + "key.pem"); err != nil {
			return nil, err
		}
		if certs[i], err = tls.X509KeyPair(caCert, keyCert); err != nil {
			return nil, err
		}
	}

	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/whoami", whoamiHandler)
	return &http.Server{
		Addr:              ":8443",
		Handler:           serveMux,
		ReadHeaderTimeout: time.Second,
		TLSConfig: &tls.Config{
			Certificates: certs,
			ClientAuth:   tls.RequireAndVerifyClientCert,
			ClientCAs:    caCertPool,
			MinVersion:   tls.VersionTLS13,
		},
	}, nil
}

func whoamiHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Hello %s\n", r.TLS.ServerName)
}
