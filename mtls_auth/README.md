## Mutual TLS Auth

### References

* [Wikipedia](https://en.wikipedia.org/wiki/Mutual_authentication#mTLS)

### Running

Generate TLS certificates

```shell
$ ./gen.sh
```

Start the server

```shell
$ go run ./server
...
```

In another shell, run the client

```shell
$ go run ./client
...
```

### Scenarios

Verify
- one client can talk successfully to two different servers
- one server can talk successfully to two different clients
- a client with the wrong cert can't talk to a server
- a 'regular' http client can't talke to the server

client c1 -> server s1
client c1 -> server s2
client c2 -> server s1
client c2 xx server s2
