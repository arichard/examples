package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"flag"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var (
	errNoCertsFound        = errors.New("no certs found")
	errTransportCastFailed = errors.New("transport cast failed")
)

func main() {
	ctx := context.Background()
	if err := client(ctx, os.Args); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	os.Exit(0)
}

func client(ctx context.Context, args []string) error {
	cmdLine := flag.NewFlagSet(args[0], flag.ExitOnError)
	var serverName string
	cmdLine.StringVar(&serverName, "s", "s1", "server name")
	var clientName string
	cmdLine.StringVar(&clientName, "c", "c1", "client name")
	err := cmdLine.Parse(args[1:])
	if err != nil {
		return err
	}
	if extraArgs := cmdLine.Args(); len(extraArgs) > 0 {
		fmt.Fprintf(cmdLine.Output(), "unexpected additional args: %v\n", extraArgs)
		cmdLine.Usage()
		os.Exit(2) //nolint:gomnd,revive // Not ideal, but OK for now
	}

	const certSuffix = "cert.pem"
	var names []string
	if names, err = filepath.Glob("*" + clientName + certSuffix); err != nil {
		return err
	}
	if len(names) == 0 {
		return errNoCertsFound
	}
	fmt.Printf("reading certs %v\n", names)
	caCertPool := x509.NewCertPool()
	certs := make([]tls.Certificate, len(names))
	for i, name := range names {
		var caCert []byte
		if caCert, err = os.ReadFile(name); err != nil {
			return err
		}
		caCertPool.AppendCertsFromPEM(caCert)
		var keyCert []byte
		if keyCert, err = os.ReadFile(strings.TrimSuffix(name, certSuffix) + "key.pem"); err != nil {
			return err
		}
		if certs[i], err = tls.X509KeyPair(caCert, keyCert); err != nil {
			return err
		}
	}

	transport, ok := http.DefaultTransport.(*http.Transport)
	if !ok {
		return errTransportCastFailed
	}
	transport = transport.Clone()
	transport.DialContext = resolve(transport.DialContext)
	transport.TLSClientConfig = &tls.Config{
		Certificates: certs,
		MinVersion:   tls.VersionTLS13,
		RootCAs:      caCertPool,
	}
	httpClient := &http.Client{
		Timeout:   time.Second,
		Transport: transport,
	}

	var req *http.Request
	if req, err = http.NewRequestWithContext(ctx, http.MethodGet, "https://"+url.PathEscape(clientName)+"."+url.PathEscape(serverName)+".localhost:8443/whoami", http.NoBody); err != nil {
		return err
	}
	var resp *http.Response
	if resp, err = httpClient.Do(req); err != nil {
		return err
	}
	fmt.Println(resp.StatusCode)
	if _, err = io.Copy(os.Stdout, resp.Body); err != nil {
		return err
	}
	return resp.Body.Close()
}

type transportDialContext func(context.Context, string, string) (net.Conn, error)

func resolve(f transportDialContext) transportDialContext {
	return func(ctx context.Context, network, addr string) (net.Conn, error) {
		// Remove clientName as it's not actually registered in DNS
		if _, rest, found := cutByte(addr, '.'); found {
			// Remove serverName because we're hacking multiple servers to the same real hostname
			if _, rest, found = cutByte(rest, '.'); found {
				addr = rest
			}
		}
		return f(ctx, network, addr)
	}
}

func cutByte(s string, sep byte) (before, after string, found bool) { //nolint:unparam // match strings.Cut
	if i := strings.IndexByte(s, sep); i >= 0 {
		return s[:i], s[i+1:], true
	}
	return s, "", false
}
