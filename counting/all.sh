#!/bin/sh
set -e

cd -- "$(dirname "$(realpath "$0")")"
go build ./

export TABLE_FORMAT=true
echo '| Name | counts/sec | Description |'
echo '|---|---|---|'
for mode in $(./counting | grep -Eo '^  [^:]*' | cut -b 3-); do
    ./counting "$mode"
done
