package main

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

type countingFunc func(*uint64)

type countingMode struct {
	f           countingFunc
	name        string
	description string
}

var modes = []*countingMode{{ //nolint:gochecknoglobals // Basically a const
	name:        "singleSimple",
	description: "single thread",
	f:           singleSimple,
}, {
	name:        "singleDeref",
	description: "single thread avoiding the ptr",
	f:           singleDeref,
}, {
	name:        "multiUnsafe",
	description: "multiple threads using unsafe adds",
	f:           multiUnsafe,
}, {
	name:        "multiChan",
	description: "multiple threads using a channel",
	f:           multiChan,
}, {
	name:        "multiMutex",
	description: "multiple threads using a mutex",
	f:           multiMutex,
}, {
	name:        "multiAtomic",
	description: "multiple threads using atomic add",
	f:           multiAtomic,
}, {
	name:        "multiBatch",
	description: "multiple threads using thread-local batches",
	f:           multiBatch,
}}

func main() {
	if len(os.Args) <= 1 {
		fmt.Printf("Usage: %s [mode]\n", os.Args[0])
		for _, mode := range modes {
			fmt.Printf("  %s: %s\n", mode.name, mode.description)
		}
		os.Exit(0)
	}
	modeName := os.Args[1]
	tableFormat, err := strconv.ParseBool(os.Getenv("TABLE_FORMAT"))
	if err != nil {
		tableFormat = false
	}
	for _, mode := range modes {
		if mode.name == modeName {
			count := uint64(0)
			go func(mode *countingMode) {
				time.Sleep(time.Second)
				countResult := atomic.LoadUint64(&count)
				printer := message.NewPrinter(language.English)
				if tableFormat {
					printer.Printf("| %s | %d | %s |\n", mode.name, countResult, mode.description)
				} else {
					printer.Printf("%s: %d\n", mode.name, countResult)
				}
				os.Exit(0)
			}(mode)
			mode.f(&count)
			os.Exit(0)
		}
	}
	fmt.Printf("Mode %s not found\n", modeName)
	os.Exit(1)
}

func singleSimple(count *uint64) {
	for {
		*count++
	}
}

func singleDeref(count *uint64) {
	for {
		c := uint64(0)
		for c < 1_000_000 {
			c++
		}
		*count += c
	}
}

func multiUnsafe(count *uint64) {
	eachCPU(func() {
		for {
			*count++
		}
	})
}

func multiChan(count *uint64) {
	ichan := make(chan uint64)
	numCPU := runtime.NumCPU()
	for i := 0; i < numCPU-1; i++ {
		go func() {
			for {
				ichan <- 1
			}
		}()
	}
	for i := range ichan {
		*count += i
	}
}

func multiMutex(count *uint64) {
	var mutex sync.Mutex
	eachCPU(func() {
		for {
			mutex.Lock()
			*count++
			mutex.Unlock()
		}
	})
}

func multiAtomic(count *uint64) {
	eachCPU(func() {
		for {
			atomic.AddUint64(count, 1)
		}
	})
}

func multiBatch(count *uint64) {
	eachCPU(func() {
		for {
			c := uint64(0)
			for c < 1_000_000 {
				c++
			}
			atomic.AddUint64(count, c)
		}
	})
}

func eachCPU(f func()) {
	numCPU := runtime.NumCPU()
	for i := 0; i < numCPU-1; i++ {
		go f()
	}
	f()
}
