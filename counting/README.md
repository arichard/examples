## Counting

How can you add by one the fastest?

### Running

```shell
$ ./all.sh
...
```

### Timing

| Name | counts/sec | Description |
|---|---|---|
| singleSimple | 4,638,659,986 | single thread |
| singleDeref | 4,647,000,000 | single thread avoiding the ptr |
| multiUnsafe | 12,403,138 | multiple threads using unsafe adds |
| multiChan | 4,516,928 | multiple threads using a channel |
| multiMutex | 14,310,005 | multiple threads using a mutex |
| multiAtomic | 108,878,149 | multiple threads using atomic add |
| multiBatch | 27,889,000,000 | multiple threads using thread-local batches |
