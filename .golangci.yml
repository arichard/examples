linters-settings:
  cyclop:
    max-complexity: 25

  depguard:
    include-go-root: true

  errcheck:
    check-type-assertions: true
    check-blank: true

  exhaustive:
    default-signifies-exhaustive: true

  funlen:
    lines: 120
    statements: 60

  gocognit:
    min-complexity: 40

  gocritic:
    enabled-tags:
      - opinionated

  govet:
    check-shadowing: true
    enable-all: true

  lll:
    line-length: 190

  nestif:
    min-complexity: 6

  nolintlint:
    allow-leading-space: false
    require-explanation: true
    require-specific: true

  revive:
    enable-all-rules: true
    rules:
      - name: add-constant
        disabled: true # overly aggressive
      - name: argument-limit
        arguments: [ 9 ]
      - name: banned-characters
        disabled: true # not worried about it
      - name: cognitive-complexity
        disabled: true # handled by other linters
      - name: confusing-naming
        disabled: true # fine having names only differ by capitalization
      - name: cyclomatic
        disabled: true # handled by other linters
      - name: defer
        disabled: true # confused by inline func in a defer
      - name: empty-lines
        disabled: true # seems confused by some struct definitions
      - name: file-header
        disabled: true # not worried about it
      - name: flag-parameter
        disabled: true # perhaps should fix but not right now
      - name: function-length
        disabled: true # handled by other linters
      - name: function-result-limit
        arguments: [ 3 ]
      - name: line-length-limit
        disabled: true # handled by other linters
      - name: max-public-structs
        disabled: true # not worried about it
      - name: nested-structs
        disabled: true # not worried about it
      - name: unhandled-error
        disabled: true # handled by other linters
      - name: unused-receiver
        disabled: true # doesn't recognize implementing interfaces

  tagliatelle:
    case:
      rules:
        json: snake
        yaml: snake

linters:
  enable-all: true
  disable:
    - deadcode # deprecated
    - exhaustivestruct # deprecated
    - exhaustruct # ok omitting values in struct initialization
    - forbidigo # disable to allow fmt
    - golint # deprecated
    - ifshort # deprecated
    - interfacer # deprecated
    - ireturn # like the general idea of returning interfaces, but likely have too many exceptions
    - maligned # deprecated
    - nlreturn # don't want newlines before returns
    - nonamedreturns # named returns have their uses
    - nosnakecase # deprecated
    - scopelint # deprecated
    - structcheck # deprecated
    - thelper # but perhaps my testing patterns aren't go-proper
    - varcheck # deprecated
    - varnamelen # disable for simplicity
    - wrapcheck # disable for simplicity
    - wsl # like code without a bunch of newlines

issues:
  exclude-use-default: true # Set to false and review from time to time
